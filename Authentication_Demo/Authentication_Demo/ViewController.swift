//
//  ViewController.swift
//  Authentication_Demo
//
//  Created by Swati Singh on 16/09/20.
//  Copyright © 2020 terralogic. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        authenticate()
        // Do any additional setup after loading the view.
    }
    func authenticate(){
           let context = LAContext()
                  var error: NSError?
                  if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthentication, error: &error) {
                      // Device can use biometric authentication
                      context.evaluatePolicy(LAPolicy.deviceOwnerAuthentication, localizedReason: "Access requires authentication",
                          reply: {(success, error) in
                              DispatchQueue.main.async {
                                  
                                  if let err = error {
                                      
                                      switch err._code {
                                          
                                      case LAError.Code.systemCancel.rawValue:
                                          self.notifyUser("Session cancelled",
                                                          err: err.localizedDescription)
                                        
                                          //exit(0)
                                        
                                      case LAError.Code.userCancel.rawValue:
                                          self.notifyUser("Please try again",
                                                          err: err.localizedDescription)
                                        //exit(0)
                                          
                                      case LAError.Code.userFallback.rawValue:
                                          self.notifyUser("Authentication",
                                                          err: "Password option selected")
        //                                  // Custom code to obtain password here
                                          
                                      default:
                                          self.notifyUser("Authentication failed",
                                                          err: err.localizedDescription)
                                      }
                                      
                                  } else {
        //                              self.notifyUser("Authentication Successful",
        //                                              err: "You now have full access")
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let directionViewController = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                    self.navigationController?.pushViewController(directionViewController, animated: true)
                                  }
                              }
                      })

                  } else {
                      // Device cannot use biometric authentication
                      if let err = error {
                          switch err.code {
                              
                          case LAError.Code.biometryNotEnrolled.rawValue:
                              notifyUser("User is not enrolled",
                                         err: err.localizedDescription)
                              
                          case LAError.Code.passcodeNotSet.rawValue:
                              notifyUser("A passcode has not been set",
                                         err: err.localizedDescription)
                              
                              
                          case LAError.Code.biometryNotAvailable.rawValue:
                              notifyUser("Biometric authentication not available",
                                         err: err.localizedDescription)
                          default:
                              notifyUser("Unknown error",
                                         err: err.localizedDescription)
                          }
                      }
                  }
    }

    
    func notifyUser(_ msg: String, err: String?) {
        let alert = UIAlertController(title: msg,
            message: err,
            preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: {action in self.authenticate()})

        alert.addAction(cancelAction)

        self.present(alert, animated: true,
                            completion: nil)
    }
}

